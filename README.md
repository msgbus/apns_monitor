# README

------
本分支包括了两块代码：
> * Java 测试 APNs 是否正常运行 
> master-->MQTTFinal-->src-->YunBaDemo.java
> * C++ 调用最新 APNs API 进行推送测试 
> master-->APNsProvider-->NewAPNsProvider-->main.cpp

系统要求：
> * Max OS 10.11.6

IDE要求：
> * Eclipse(Java)
> * Xcode 7.0

Java 测试 APNs 模块原理：
> * 测试模块和 iOS 客户端订阅同一个 topic ，假设是 testtopic ，测试模块调用 API 向 testtopic 进行 publish ，假设发送了一条推送 testmsg ，iOS 客户端接收到后，会向一个默认 topic 发送一条确认收到的消息，假设是 alreadyAccept ，测试模块已经默认 subscribe 了该 topic ，因此会接收到 alreadyAccept 消息，由此进行 APNs 的监测，如果没有收到则表示 iOS 客户端没有接收到推送


C++ 调用 APNs API ：
> * APNs API 要求使用 http/2 协议进行请求，同时 https 需要 SSL 进行证书验证，Xcode 需要配置 P12 证书并转换成 PEM 格式，更新 curl + openssl 才能支持 http2 请求。


------

## 测试步骤

### 1. Java 准备
下载 MQTTFinal.zip 至本地，用eclipse导入解压后的文件夹，只有一个主要的类 YunBaDemo.java

### 2. iOS 准备

下载 YunBaDemo.zip ，双击 YunBaDemo.xcodeproj 打开工程，需要配置真机测试证书，然后安装到 iphone 上，需要将 是否接受推送 按钮处于开启状态

### 3. Yunba.io 准备
登陆 Yunba.io ，创建一个应用，得到 Appkey ，并且配置推送证书，在这里不做赘述

### 4. 进行测试
现在在 Eclipse 上运行 YunBaDemo.java ，按照格式规范输入（注意：需要在iphone的 App 上设置订阅一个 topic ）， App 上的 topic 需要与输入命令中的 topic 一致，输入完毕后回车即可

输入格式要求
```java
-i <Appkey> -t <topic> -m <msg>
```
样例输入
```java
-i 578d8b3db4c9e656789d1e02 -t day1 -m msgtest
```

### 5. 测试结果
APNs 正常运行
```java
iOS reply finished, APNs running successful
```
APNs 运行错误
```java
iOS reply out of time, APNs crash
```