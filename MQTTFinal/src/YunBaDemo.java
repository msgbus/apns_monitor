import java.io.UnsupportedEncodingException;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttAsyncClient;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;

public class YunBaDemo {
	private static String Appkey;
	private static String topic;
	private static String msg;
	private static Scanner input;
	private static boolean TimeFlag;
	private static long time;
	private static int flag;
	private static int deadtime;//超时时常(s)
	public static void main(String[] args) throws InterruptedException,  
    ExecutionException  {
		//测试入口
		RootFunction();
	}
	public static void RootFunction() throws InterruptedException,  
    ExecutionException {
		
		System.out.println("please input information with format like -i <Appkey> -t <topic> -m <msg>");
		System.out.print("command : ");
		String command = null;
		input = new Scanner(System.in);
		command = input.nextLine();
		input.close();
		
		ParseCommand(command);
		CountDown();
	}
	/*
	 * 解析command
	 */
	public static void ParseCommand(String command){
		//解析command
		int i = 0;
		for(i=0;i<command.length();i++){
			if(command.charAt(i) == '-' && command.charAt(i+1) == 'i'){
				Appkey = command.substring(i+3,i+27);
				command = command.substring(i+28,command.length());
			}
			else if(command.charAt(i) == '-' && command.charAt(i+1) == 'm'){
				topic = command.substring(0,i-1);
				topic = topic.substring(3, i-1);
				command = command.substring(i+3,command.length());
				msg = command;
	     	}
		}
	}
	/*
	 * 计时
	 */
	public static void CountDown(){
		time = 1 * 5; // 自定义倒计时时间
        long hour = 0;
        long minute = 0;
        long seconds = 0;
        TestAPNs();
        TimeFlag = true;
        while (time >= 0) {
        	if(time != 5)
        		System.out.print("##### ");
        	if(flag == 1){
        		System.out.println();
            	System.out.println("iOS reply finished, APNs running successful");
            	System.exit(0);
            }
            hour = time / 3600;
            minute = (time - hour * 3600) / 60;
            seconds = time - hour * 3600 - minute * 60;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }   
            time--;
        }
        if(flag != 1){
        	System.out.println();
        	System.out.println("iOS reply out of time,APNs crash");
        	System.exit(0);
        }   
	}
	/*
	 * 连接yunba服务器，publish & subscribe 
	 */
	public static void TestAPNs(){
		//测试模块
		try {
			final MqttAsyncClient mqttAsyncClient = MqttAsyncClient.createMqttClient(Appkey);
		    //连接云巴服务器
			mqttAsyncClient.connect(new IMqttActionListener() {
				@Override
				public void onSuccess(IMqttToken arg0) {
					System.out.println("already connect to yunba successfully");
					try {
                        //监听YunBaTopic
						mqttAsyncClient.subscribe("YunbaTopic", 1, null, new IMqttActionListener() {
							//连接成功
							@Override
							public void onSuccess(IMqttToken asyncActionToken) {
								try {
							    	//将topic[xxxx]作为publish的目的地
									mqttAsyncClient.publish(topic, msg.getBytes(), 1, false,
                                        null, new IMqttActionListener() {
                                        @Override
                                        public void onFailure(IMqttToken arg0, Throwable arg1) {
                                            System.out.println("msg sending failed");
                                        }
                                        @Override
                                        public void onSuccess(IMqttToken arg0) {
                                        }
									});
								} catch (MqttPersistenceException e) {
									e.printStackTrace();
								} catch (MqttException e) {
									e.printStackTrace();
								}
							}
							//连接失败
							@Override
							public void onFailure(IMqttToken asyncActionToken,Throwable exception) {
								if (exception instanceof MqttException) {
									MqttException ex = (MqttException)exception;
									System.err.println("connect to server failed with the error code = " + ex.getReasonCode());
								}
							}
						});
					} catch (Exception e) {
						e.printStackTrace();
					}
					try {
						mqttAsyncClient.setAlias("java_alias", new IMqttActionListener() {
							@Override
							public void onSuccess(IMqttToken arg0) {
								try {
                                    mqttAsyncClient.getAlias(new IMqttActionListener() {
										@Override
										public void onSuccess(IMqttToken token) {
										}
										@Override
										public void onFailure(IMqttToken arg0, Throwable arg1) {
											if(arg1 instanceof MqttException) {
												MqttException mqtt = (MqttException)arg1;		
											}
										}
									});
									mqttAsyncClient.publishToAlias("java_alias", "msg to java_alaias", null);
									mqttAsyncClient.getTopics("java_alias", new IMqttActionListener() {			
										@Override
										public void onSuccess(IMqttToken token) {
										}
										@Override
										public void onFailure(IMqttToken arg0, Throwable arg1) {	
										}
									});
								} catch (MqttPersistenceException e) {
									e.printStackTrace();
								} catch (UnsupportedEncodingException e) {
									e.printStackTrace();
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
							@Override
							public void onFailure(IMqttToken arg0, Throwable arg1) {	
							}
						});
					} catch (Exception e) {
						e.printStackTrace();
					}	
				}
				@Override
				public void onFailure(IMqttToken arg0, Throwable arg1) {
					System.out.println("mqtt connect failed" + arg1.getMessage());
				}
			});
			//接收模块
			mqttAsyncClient.setCallback(new MqttCallback() {
				@Override
				public void messageArrived(String topic, MqttMessage message) throws Exception {
					String result = new String(message.getPayload());
					if(result.equals("已经收到推送") && flag == 0){//5秒内收到确认回复
						System.out.println("reply from iOS : " + result);
						flag = 1;
					}
				}
				@Override
				public void deliveryComplete(IMqttDeliveryToken token) {
				}
				@Override
				public void connectionLost(Throwable cause) {
					System.out.println("mqtt connectionLost");
				}
				@Override
				public void presenceMessageArrived(String topic,
						MqttMessage message) throws Exception {
					System.out.println("presenceMessageArrivedtopic = " + topic + " msg = " + new String(message.getPayload())) ;
				}
			});	
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static <T> String join(T[] array, String cement) {
	    StringBuilder builder = new StringBuilder();
	    if(array == null || array.length == 0) {
	        return null;
	    }
	    for (T t : array) {
	        builder.append(t).append(cement);
	    }
	    builder.delete(builder.length() - cement.length(), builder.length());
	    return builder.toString();
	}
}
