//
//  main.cpp
//  NewAPNsProvider
//
//  Created by 安达 on 16/8/1.
//  Copyright © 2016年 安达. All rights reserved.
//

#include <iostream>
#include <curl/curl.h>
#include "json.h"

#define URL                 "https://api.development.push.apple.com:443/3/device/" //需要 libcurl 7.46+ 版本
#define deviceToken         "b22b405dace2bd3dd14d8449ab0e52d387391de1d0e9506b92037fb03a2bb0a3"


int main(int argc, const char * argv[]) {
    
    //https 需要加密文件进行验证
    //http  不需要加密文件
    
    /*
     * json参数字段
     */
    Json::FastWriter writer;
    Json::Value json_temp;
    Json::Value arrayObject;
    arrayObject["alert"] = "test message today";
    arrayObject["badge"] = 1;
    arrayObject["sound"] = "default";
    json_temp["aps"] = arrayObject;
    std::string json = writer.write(json_temp);                         //将json格式转成string
    
    /*
     * 初始化 CURL 对象
     */
    CURL * curl = curl_easy_init();
    
    if(curl){
        //curl_easy_setopt(curl, CURLOPT_TIMEOUT , 10);设置超时
        
        curl_easy_setopt(curl, CURLOPT_VERBOSE , 1L);                       //打印调试信息
        curl_slist * http_headers;
        http_headers = curl_slist_append(http_headers, "Accept:application");
        http_headers = curl_slist_append(http_headers, "Content-Type:application/json");
        http_headers = curl_slist_append(http_headers, "charsets:utf-8");
        http_headers = curl_slist_append(http_headers, "apns-expiration:1");
        http_headers = curl_slist_append(http_headers, "apns-priority:10");
        http_headers = curl_slist_append(http_headers, "apns-topic:com.myapns");
        
        curl_easy_setopt(curl,CURLOPT_POST,1);                              //设置为非0表示本次操作为post
        curl_easy_setopt(curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_2_0);//HTTP/2协议
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, http_headers);           //添加 httpheader
        
        
        std::string url = URL;
        std::string devicetoken = deviceToken;
        std::string posturl = url + devicetoken;                            //拼接 http 请求路径
        
        /*
         * SSL设置
         */
        curl_easy_setopt(curl, CURLOPT_URL,posturl.c_str());
        curl_easy_setopt(curl, CURLOPT_SSLCERTTYPE, "PEM");                 //设置CERT证书类型,openssl 1.0.2才能支持pem格式文件
        curl_easy_setopt(curl, CURLOPT_SSLCERT, "/Users/anda/Desktop/MyAPNs20160802.pem");
        curl_easy_setopt(curl, CURLOPT_SSLCERTPASSWD, "950608");            //证书密码
        
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS,json.c_str());            //携带json参数进行post
        
        
        /*
         * 执行请求
         */
        CURLcode response = curl_easy_perform(curl);
        
        /* 
         * 检查错误
         */
        fprintf(stderr, "curl_easy_perform() failed: %s\n",
                    curl_easy_strerror(response));
        
        /*
         * 资源释放
         */
        curl_slist_free_all(http_headers);
        curl_easy_cleanup(curl);
    }
    return 0;
}















