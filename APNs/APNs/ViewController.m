//
//  ViewController.m
//  APNs
//
//  Created by 安达 on 16/7/29.
//  Copyright © 2016年 安达. All rights reserved.
//

#import "ViewController.h"
#define Screen_Width      [[UIScreen mainScreen]bounds].size.width
@interface ViewController (){
    UIView * containerView;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithRed:245.0/255 green:245.0/255 blue:245.0/255 alpha:1]];
    
    containerView = [[UIView alloc]init];
    [containerView setFrame:CGRectMake(Screen_Width * 0.1, 100, Screen_Width*0.8, 300)];
    containerView.backgroundColor = [UIColor whiteColor];
    containerView.layer.cornerRadius = 4;
    containerView.layer.shadowColor = [UIColor grayColor].CGColor;
    [self.view addSubview:containerView];
    
    _textView = [[UITextView alloc]init];
    [_textView setFrame:CGRectMake(Screen_Width * 0.05, 5, Screen_Width * 0.7, 280)];
    _textView.text = @"dsadas";
    _textView.textColor = [UIColor grayColor];
    [containerView addSubview:_textView];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
